import json
import os
import boto3
from botocore.exceptions import ClientError


def lambda_handler(event, context):
    # TODO implement
    message = '{ "name":"John", "age":30}'
    subject = "From  Lambda"
    message_id = send_sns(message, subject)
    
    print(
        f'Message published - with message Id - {message_id}.'
    )
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }


def send_sns(message, subject):
    
    client = boto3.client("sns")
    topic_arn = os.environ["SNS_ARN"]
    
    print(f'Publishing message to topic - {topic_arn}...')
    
    try:
        
        response = client.publish(
        TopicArn=topic_arn, 
        Message=message,
        MessageStructure="string",
        Subject=subject
    )['MessageId']
    
    except ClientError:
        print(f'Could not publish message to the topic.')
        print(ClientError)
        raise
    
    else:
        return response