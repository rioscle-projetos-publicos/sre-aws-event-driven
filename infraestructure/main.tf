terraform {

  required_version = "0.14.4"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.49.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

### criação da infraestrutura

module "create-infrastructure" {
  source = "./modules"
  topic_name = "sns-principal_topic"
  queue_name = "queue-principal_sqs"
  queue_dlq_name = "queue-principal_dlq"
}
