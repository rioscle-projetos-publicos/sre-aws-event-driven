variable "environment" {
    type        = string
    description = "Nome do Ambiente"
    default     = "dev"
}

### Variables for sns
variable "topic_name" {
    description = "The name of the SNS topic to create"
    type        = string
}

### Variables for sqs and dlq
variable "queue_name" {
    description = "queue name"
    type        = string
}

variable "queue_delay_seconds" {
    description = "queue delay secounds"
    type        = number
    default     = 90
}

variable "queue_max_message_size" {
    description = "queue max message size"
    type        = number
    default     = 2048
}

variable "queue_message_retention_seconds" {
    description = "queue message retention seconds"
    type        = number
    default     = 60
}

variable "queue_receive_wait_time_seconds" {
    description = "queue receive wait time seconds"
    type        = number
    default     = 10
}

variable "queue_maxReceiveCount" {
    description = "queue maxReceiveCount"
    type        = number
    default     = 4
}

variable "queue_dlq_name" {
    description = "The name of dlq"
    type        = string
}

