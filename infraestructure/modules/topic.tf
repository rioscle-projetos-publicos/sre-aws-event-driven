resource "aws_sns_topic" "create-topic" {
  name = var.topic_name
  tags = local.common_tags
}