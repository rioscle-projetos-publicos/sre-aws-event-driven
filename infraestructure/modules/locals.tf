locals {
  common_tags = {
    Project     = "SRE-AWS-EVENT-DRIVEN"
    Manegment   = "terraform"
    Environment = var.environment
  }
}