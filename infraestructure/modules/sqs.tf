resource "aws_sqs_queue" "create-queue" {
  name                      = var.queue_name
  delay_seconds             = var.queue_delay_seconds
  max_message_size          = var.queue_max_message_size
  message_retention_seconds = var.queue_message_retention_seconds
  receive_wait_time_seconds = var.queue_receive_wait_time_seconds
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.create_queue_deadletter.arn
    maxReceiveCount     = var.queue_maxReceiveCount
  })

  tags = local.common_tags
}

resource "aws_sqs_queue" "create_queue_deadletter" {
  name = var.queue_dlq_name
}

resource "aws_sns_topic_subscription" "sns_topic_subscription_sqs_target" {
  topic_arn = aws_sns_topic.create-topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.create-queue.arn
}

# policty to permit SNS send message to SQS
resource "aws_sqs_queue_policy" "results_updates_queue_policy" {
    queue_url = "${aws_sqs_queue.create-queue.id}"

    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "${aws_sqs_queue.create-queue.arn}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.create-topic.arn}"
        }
      }
    }
  ]
}
POLICY
}