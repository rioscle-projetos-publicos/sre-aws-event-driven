variable "aws_region" {
  type        = string
  description = "Regiao da AWS onde a infraestrutura será criada"
  default     = "us-east-1"
}