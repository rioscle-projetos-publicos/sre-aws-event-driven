# sre-aws-event-driven

## Introdução

O objetivo deste projeto é subir na AWS, através do terraform uma arquitetura **Event Driven**.

Com esta arquitetura estaremos estudando as métricas para desenvolver um Observabilidade que seja capaz de detectar problema ou possíveis indicíos.

## Desenho de Arquitetura

![AWS event driven architecture](/images/sre-aws-event-driven.png "sre-aws-event-driven")

## Itens de Configuração

### Lambda - post-event-topic_function
Este lambda tem por responsabilidade postar eventos no tópico.
Com estes eventos estaremos simulando situações do dia a dia

### SNS - events-principal_topic
Este tópico irá receber os eventos gerados pelo lambda **post-event-topic_function**

### SQS - queue-1_queue
Esta fila irá receber os eventos do tópico **principal_topic**

**DLQ - queue-1_dlq**: Esta fila dlq irá armazenar os eventos desprezados

### Lambda - read-queue_function
Este lambda irá receber os eventos da fila **queue-1_queue**

## Documentação

### SNS

- Terraform: [link](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic)
- AWS: [link](https://docs.aws.amazon.com/pt_br/sns/latest/dg/welcome.html)

### SQS

- Terraform: [link](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue)
- AWS: [link](https://docs.aws.amazon.com/pt_br/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html)
- AWS: Configurações da Fila: [link](https://docs.aws.amazon.com/pt_br/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-configure-queue-parameters.html)

## Documentação das Métricas de SQS
https://docs.aws.amazon.com/pt_br/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-available-cloudwatch-metrics.html

# Número aproximado de mensagens visíveis
Métrica: SQS ApproximateNumberOfMessagesVisible
Descrição: Mostra a quantidade de mensagens visiveis na fila e que ainda não foram processadas

# Número de mensagens enviadas
Métrica: SQS NumberOfMessagesSent
Descrição: Quantidade de Mensagens que foram enviadas/adicionadas a fila

# Número de mensagens recebidas
Métrica: NumberOfMessagesReceived
Descrição: 
  Quantidade de Mensagens que saíram na fila ( exemplo: mensagem processada pelo lambda). 
  Muito perecido com a "Mensagens enviadas", no entanto, se a mensagem retornou para a fila ( tipo, se o lambda não conseguiu processar ), este métricas é contada
  É contato a retentativas de processar a fila.

# Número de mensagens excluídas
Métrica: NumberOfMessagesDeleted
Descrição: Quantidade de mensagens que foram excluídas ( saíra da fila ) após o processamento




### Inscrever uma fila em um tópico

- Terraform: [link](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription)
- AWS: [link](https://docs.aws.amazon.com/pt_br/sns/latest/dg/sns-sqs-as-subscriber.html)

### Criar lambda para criar eventos em um topico SNS
- Exemplo de projeto para criar um lambda: [link](https://github.com/HaissamHammoud/terraform-examples)
- SDK Boto3 Python SNS Publish:[link](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html#SNS.Client.publish)
- Exemplo 1: [link](https://awstip.com/snippet-of-amazon-sns-in-aws-lambda-python-6504995deaaf)